package com.api.API;

import org.testng.AssertJUnit;
//import org.testng.AssertJUnit;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;


public class App 
{
	 int statusCode;

	/* @Test
	    public void getAllUserDetailsTest() {
	        RestAssured.baseURI = "https://reqres.in/api/users/";
	        
	        RequestSpecification httpRequest = RestAssured.given();

	        Response response = httpRequest.request(Method.GET, "");

	        String responseBody = response.getBody().asString();
	        System.out.println("Response Body: "+responseBody);
	        System.out.println("Response Time: "+response.getTime());
	        statusCode = response.getStatusCode();
	    }*/
	 
	   @Test
	    public void getUserDetailsTest() {
	        RestAssured.baseURI = "https://reqres.in/api/users/";
	        
	        RequestSpecification httpRequest = RestAssured.given();

	        Response response = httpRequest.request(Method.GET, "2");

	        // Response response = get("http://generator.swagger.io/api/gen/download/");

	        String responseBody = response.getBody().asString();
	        System.out.println("Response Body: "+responseBody);
	        System.out.println("Response Time: "+response.getTime());
	        statusCode = response.getStatusCode();
	        JsonPath jsonPathEvaluator = response.jsonPath();
	        
	        System.out.println("Email received from Response " + jsonPathEvaluator.getString("data.email"));
	        
	        AssertJUnit.assertEquals("Email Address ", "janet.weaver@reqres.in", jsonPathEvaluator.getString("data.email"));
	        AssertJUnit.assertEquals("First Name ", "Janet", jsonPathEvaluator.getString("data.first_name"));
	        AssertJUnit.assertEquals("Last Name ", "Weaver", jsonPathEvaluator.getString("data.last_name"));
	       
	    }
}
